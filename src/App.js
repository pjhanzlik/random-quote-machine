import React, {useState} from 'react';
import './App.css';
import TweetButton from './components/TweetButton';
import QuoteMark from './components/QuoteMark';
import { sanitize } from 'dompurify';


/* Quotes courtesy of https://en.wikiquote.org/wiki/Special:ApiSandbox#action=parse&format=json&page=Beginnings&prop=text&callback=
	- parsed with new RegExp(`<ul><li>(.*?)<ul>(.*?)</ul></li></ul>`, 'g') and then
	- find <ul><li>(.*?)<ul><li>(.*?)</li></ul></li></ul>,  replace with "\1": "\2", and then
	- find </li>(.*)<li>, replace with <br />
	- find href=\"/, replace with href=\"https://en.wikiquote.org/
*/
const CITATIONS = {
	"End, begin, all the same. Big change. Sometimes good, sometimes bad.\n": "Aughra, voiced by Billie Whitelaw in <i><a href=\"https://en.wikiquote.org/wiki/The_Dark_Crystal\" title=\"The Dark Crystal\">The Dark Crystal</a></i> (1982), directed by <a href=\"https://en.wikiquote.org/wiki/Jim_Henson\" title=\"Jim Henson\">Jim Henson</a> and Frank Oz",
	"<b>What one needs to do at every <a href=\"https://en.wikiquote.org/wiki/Moment\" title=\"Moment\">moment</a> of one's <a href=\"https://en.wikiquote.org/wiki/Life\" title=\"Life\">life</a> is to put an <a href=\"https://en.wikiquote.org/wiki/End\" title=\"End\">end</a> to the old <a href=\"https://en.wikiquote.org/wiki/World\" title=\"World\">world</a> and to <a href=\"https://en.wikiquote.org/wiki/Begin\" class=\"mw-redirect\" title=\"Begin\">begin</a> a new world.</b>\n": "<a href=\"https://en.wikiquote.org/wiki/Nikolai_Berdyaev\" title=\"Nikolai Berdyaev\">Nikolai Berdyaev</a>, <i>The Beginning and the End</i> (1947).",
	"The White Rabbit put on his spectacles. 'Where shall I begin, please your Majesty?' he asked.<br />'Begin at the beginning,' the King said gravely, 'and go on till you come to the end: then stop.'\n": "<a href=\"https://en.wikiquote.org/wiki/Lewis_Carroll\" title=\"Lewis Carroll\">Lewis Carroll</a>, <i><a href=\"https://en.wikiquote.org/wiki/Alice%27s_Adventures_in_Wonderland\" title=\"Alice&#39;s Adventures in Wonderland\">Alice's Adventures in Wonderland</a></i> (1865).",
	"<i>In omnibus autem negotiis priusquam adgrediare, adhibenda est praeparatio diligens.</i>\n": "In all matters, before beginning, a diligent preparation should be made.<br /><a href=\"https://en.wikiquote.org/wiki/Cicero\" title=\"Cicero\">Cicero</a>, <i>De Officiis</i> (44 B.C.), I. 21.",
	"Like the legend of the Phoenix <br /> All ends with beginnings <br /> What keeps the planets spinning (uh) <br /> The force from the beginning.\n": "<a href=\"https://en.wikiquote.org/wiki/Daft_Punk\" title=\"Daft Punk\">Daft Punk</a> <i><a href=\"https://en.wikipedia.org/wiki/Random_Access_Memories\" class=\"extiw\" title=\"w:Random Access Memories\">Random Access Memories</a></i> <i>Get Lucky</i>",
	"<i>La distance n'y fait rien; il n'y a que le premier pas qui co\u00fbte.</i>\n": "The distance is nothing; it is only the first step that costs.<br /><a href=\"https://en.wikiquote.org/wiki/Marie_Anne_de_Vichy-Chamrond,_marquise_du_Deffand\" title=\"Marie Anne de Vichy-Chamrond, marquise du Deffand\">Marie Anne de Vichy-Chamrond, marquise du Deffand</a>, letter to d'Alembert, July 7, 1763. See also <a href=\"https://en.wikiquote.org/wiki/Edward_Gibbon\" title=\"Edward Gibbon\">Edward Gibbon</a>, <i>Decline and Fall of the Roman Empire</i>, Chapter XXXIX. N. 100. Phrase \"C'est le premier pas qui co\u00fbte\" attributed to Cardinal Polignac.",
	"A bad beginning makes a bad ending.\n": "<a href=\"https://en.wikiquote.org/wiki/Euripides\" title=\"Euripides\">Euripides</a> <i>\u00c6olus</i>, Frag. 32.",
	"<i>Et redit in nihilum quod fuit ante nihil.</i>\n": "It began of nothing and in nothing it ends.<br /><a href=\"https://en.wikiquote.org/w/index.php?title=Cornelius_Gallus&amp;action=edit&amp;redlink=1\" class=\"new\" title=\"Cornelius Gallus (page does not exist)\">Cornelius Gallus</a>, translated by <a href=\"https://en.wikiquote.org/wiki/Robert_Burton\" title=\"Robert Burton\">Robert Burton</a> in <i>Anatomy of a Melancholie</i> (1621).",
	"All beginnings are very troublesome things.\n": "<a href=\"https://en.wikiquote.org/wiki/Letitia_Elizabeth_Landon\" title=\"Letitia Elizabeth Landon\">Letitia Elizabeth Landon</a>, Traits and Trials of Early Life (1836), <i>The History of Mabel Dacre's First Lessons</i>.",
	"The only joy in the world is to begin. It is good to be alive because living is beginning, always, every moment. When this sensation is lacking\u2014as when one is in prison, or ill, or stupid, or when living has become a habit\u2014one might as well be dead.\n": "<a href=\"https://en.wikiquote.org/wiki/Cesare_Pavese\" title=\"Cesare Pavese\">Cesare Pavese</a>, <i>This Business of Living</i>, <span class=\"mw-formatted-date\" title=\"1937-11-23\">1937-11-23</span>",
	"Things bad begun make strong themselves by ill.\n": "<a href=\"https://en.wikiquote.org/wiki/William_Shakespeare\" title=\"William Shakespeare\">William Shakespeare</a>, <i><a href=\"https://en.wikiquote.org/wiki/Macbeth\" title=\"Macbeth\">Macbeth</a></i> (1605), Act III, scene 2, line 56.",
	"The true beginning of our end.\n": "<a href=\"https://en.wikiquote.org/wiki/William_Shakespeare\" title=\"William Shakespeare\">William Shakespeare</a>, <i><a href=\"https://en.wikiquote.org/wiki/A_Midsummer_Night%27s_Dream\" title=\"A Midsummer Night&#39;s Dream\">A Midsummer Night's Dream</a></i> (c. 1595-96), Act V, scene 1, line 111.",
	"<i>Incipe; dimidium facti est c\u0153pisse. Supersit<br />Dimidium: rursum hoc incipe, et efficies.</i>\n": "Begin; to begin is half the work. Let half still remain; again begin this, and thou wilt have finished.<br /><a href=\"https://en.wikiquote.org/wiki/Ausonius\" title=\"Ausonius\">Ausonius</a>, <i>Epigrams</i>, LXXXI. 1.",
	"<i>Incipe quidquid agas: pro toto est prima operis pars.</i>\n": "Begin whatever you have to do: the beginning of a work stands for the whole.<br /><a href=\"https://en.wikiquote.org/wiki/Ausonius\" title=\"Ausonius\">Ausonius</a>, <i>Idyllia</i>, XII. Inconnexa. 5.",
	"<i>Il n'y a que le premier obstacle qui co\u00fbte \u00e0 vaincre la pudeur.</i>\n": "It is only the first obstacle which counts to conquer modesty.<br /><a href=\"https://en.wikiquote.org/wiki/Jacques-B%C3%A9nigne_Bossuet\" title=\"Jacques-B\u00e9nigne Bossuet\">Jacques-B\u00e9nigne Bossuet</a>, Pens\u00e9es Chr\u00e9tiennes et Morales. LX.",
	"<i>Omnium rerum principia parva sunt.</i>\n": "The beginnings of all things are small.<br /><a href=\"https://en.wikiquote.org/wiki/Cicero\" title=\"Cicero\">Cicero</a>, <i>De Finibus Bonorum et Malorum</i>, V. 21.",
	"<i>Dimidium facti qui c\u0153pit habet.</i>\n": "What's well begun, is half done.<br /><a href=\"https://en.wikiquote.org/wiki/Horace\" title=\"Horace\">Horace</a>, <i>Epistles</i>,  I. 2. 40. (Traced to Hesiod).",
	"<i>C\u0153pisti melius quam desinis. Ultima primis cedunt.</i>\n": "Thou beginnest better than thou endest. The last is inferior to the first.<br /><a href=\"https://en.wikiquote.org/wiki/Ovid\" title=\"Ovid\">Ovid</a>, <i>Heroides</i>, IX. 23.",
	"<i>Principiis obsta: sero medicina paratur,<br />Cum mala per longas convaluere moras.</i>\n": "Resist beginnings: it is too late to employ medicine when the evil has grown strong by inveterate habit.<br /><a href=\"https://en.wikiquote.org/wiki/Ovid\" title=\"Ovid\">Ovid</a>, <i>Remedia Amoris</i>, XCI.",
	"<i>Deficit omne quod nascitur.</i>\n": "Everything that has a beginning comes to an end.<br /><a href=\"https://en.wikiquote.org/wiki/Quintilian\" title=\"Quintilian\">Quintilian</a>, <i>De Institutione Oratoria</i>, V. 10.",
	"<i>Quidquid c\u0153pit, et desinit.</i>\n": "Whatever begins, also ends.<br /><a href=\"https://en.wikiquote.org/wiki/Seneca_the_Younger\" title=\"Seneca the Younger\">Seneca the Younger</a>, <i>De Consolatione ad Polybium</i>, I.",
	"<i>C'est le commencement de la fin.</i>\n": "It is the beginning of the end.<br />Ascribed to <a href=\"https://en.wikiquote.org/wiki/Charles_Maurice_de_Talleyrand-P%C3%A9rigord\" title=\"Charles Maurice de Talleyrand-P\u00e9rigord\">Charles Maurice de Talleyrand-P\u00e9rigord</a> in the <i>Hundred Days</i>. Also to Gen. Augereau (1814).",
	"<i>Le premier pas, mon fils, que l'on fait dans le monde,<br />Est celui dont d\u00e9pend le reste de nos jours.</i>\n": "The first step, my son, which one makes in the world, is the one on which depends the rest of our days.<br /><a href=\"https://en.wikiquote.org/wiki/Voltaire\" title=\"Voltaire\">Voltaire</a>, <i>L'Indiscret</i>, I, 1."
}
const QUOTES = Object.keys(CITATIONS);
const SOURCE = [
	'Wikiquote contributors, "Beginnings," Wikiquote (accessed October 3, 2019)',
	"https://en.wikiquote.org/w/index.php?title=Beginnings&oldid=2670789"
];

export default function App(props) {
	const [quote, setQuote] = useState(
		QUOTES[Math.floor(Math.random() * QUOTES.length)]
	);

	const getRandomQuote = () => {
		return QUOTES[Math.floor(Math.random() * QUOTES.length)];
	}

	const getRandomColor = () => {
		return `hsl(${Math.floor(Math.random() * 361)},40%,40%)`;
	}

	const createMarkup = (html) => {
		return {__html: sanitize(html)};
	}

	const createText = (html) => {
		return html.replace(/<.*?>/g, '');
	}

	const handleTimeout = () => {
		setQuote(getRandomQuote());
		document.documentElement.style.setProperty('--secondary-color', 'white');
		document.documentElement.style.setProperty('--primary-color', getRandomColor());
		document.getElementById('new-quote').removeAttribute('disabled');
	}

	const handleClick = () => {
		document.documentElement.style.setProperty('--secondary-color', 'transparent');
		document.getElementById('new-quote').setAttribute('disabled', 'true');
		window.setTimeout(handleTimeout, 1000);
	};


	document.documentElement.style.setProperty('--primary-color', getRandomColor());	
	return (
		<div id="app">
			<div id="quote-box">
				<QuoteMark width="16px" height="16px" id="mark" />
				<blockquote id="text" dangerouslySetInnerHTML={createMarkup(quote)} />
				<cite id="author" dangerouslySetInnerHTML={createMarkup(CITATIONS[quote])} />
				<button id="new-quote" onClick={handleClick}>New quote</button>
				<TweetButton cite={createText(CITATIONS[quote])} quote={createText(quote)} />
			</div>
			<a id="source" href={SOURCE[1]}>{SOURCE[0]}</a>
		</div>
	);
}
